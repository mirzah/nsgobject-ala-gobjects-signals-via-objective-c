#import "NSGObject.h"

//
// A simple message receiver
//
@interface NSGObjectMessageReceiver : NSObject
{
    id  _target;
    SEL _selector;
    id  _data;
}
@property (assign, readwrite) id target, data;
@property (assign, readwrite) SEL selector;
@end

@implementation NSGObjectMessageReceiver

@synthesize target = _target;
@synthesize data = _data;
@synthesize selector = _selector;
@end


@implementation NSGObject
//
// Create message and method signature dictionary
//
- (id) init
{
    if(nil != (self = [super init]))
    {
        msgTargetDict    = [NSMutableDictionary new];
        msgMethodSigDict = [NSMutableDictionary new]; 

        [self installMessage:@"destroy" selector:@selector(destroy:data:)];
    }

    return self;
}

//
// Compare methods signature 'sig1' and 'sig2'
//
- (BOOL) compareMethodsSignature:(NSMethodSignature *)sig1 sig2:(NSMethodSignature *)sig2
{
    NSUInteger n1 = [sig1 numberOfArguments]; 
    NSUInteger n2 = [sig2 numberOfArguments]; 

    if(n1 == n2)
    {
        // compare return type
        const char *a1 = [sig1 methodReturnType]; 
        const char *a2 = [sig2 methodReturnType];

        if(0 != strcmp(a1,a2))
        {
            NSLog(@"The methods differ in return type: '%s' vs. '%s' \n", a1, a2);
            return NO;
        }

        // compare argument type
        for(NSUInteger i=2; i<n1; ++i)
        {
            a1 = [sig1 getArgumentTypeAtIndex:i]; 
            a2 = [sig2 getArgumentTypeAtIndex:i];
    
            if(0 != strcmp(a1,a2))
            {
                NSLog(@"The methods differ in argument no. %lu, of type: '%s' vs. '%s' \n", i, a1, a2);
                return NO;
            }
        }
    }else
    {
        NSLog(@"The methods differ in number of arguments %lu vs. %lu.\n", n1, n2);
        return NO;
    }

    return YES;
}

//
// Emit message of type: message, arg1, arg2 ...
//
- (void) emitMessage:(NSString *)message, ... 
{
    if(msgTargetDict && msgMethodSigDict) // can be nil if object is 'destroy'-ed explicit by subclass
    {
        NSMutableArray *targets = [msgTargetDict objectForKey:message];

        if(nil != targets)
        {
            NSEnumerator      *enumTargets = [targets objectEnumerator]; 
            NSMethodSignature *sig         = [msgMethodSigDict objectForKey:message]; 
            NSInvocation      *invocation  = [NSInvocation invocationWithMethodSignature:sig]; 
            id                 receiver    = nil;
            BOOL               result      = NO;
            id                 argObject   = nil;
            va_list            argumentList;

            while((receiver = [enumTargets nextObject]) != nil)
            {
                NSUInteger index = 2;
                NSUInteger nArgs = [sig numberOfArguments];

                [invocation setTarget:[receiver target]];
                [invocation setSelector:[receiver selector]];
                
                // pack arguments
                va_start(argumentList, message); 
                while(nil != (argObject = va_arg(argumentList, id)))
                {
                    // check for reservation
                    if(index+1 >= nArgs)
                    {
                        NSLog(@"Too much arguments provided to emit message '%@'.\
                              Ignoring after: %lu !\n", message, index);
                        break;
                    }

                    [invocation setArgument:&argObject atIndex:index++];
                }
                va_end(argumentList);
                
                id data = [receiver data];

                // if provided, pack 'data' as last argument (but not for class selector)
                if(nil != data && data != sig)
                    [invocation setArgument:data atIndex:index];

                [invocation invoke];
                [invocation getReturnValue:&result];

                if(result == YES) break;
            }
        }else
            NSLog(@"Trying to emit non-existing message '%@'. Aborting!\n", message);
    }
}

- (BOOL) destroy:(id)object data:(id)data
{
    if(msgTargetDict && msgMethodSigDict)
    {
        // release everything
        NSEnumerator *enumMessages = [msgTargetDict keyEnumerator];
        id            message      = nil;

        while((message = [enumMessages nextObject]) != nil) 
        {
            id targets = [msgTargetDict objectForKey:message];
            
            NSEnumerator             *enumTargets = [targets objectEnumerator]; 
            NSGObjectMessageReceiver *receiver    = nil;

            while((receiver = [enumTargets nextObject]) != nil)
                [receiver release];

            [targets release];
        }
        
        [msgTargetDict    release];
        [msgMethodSigDict release];

        msgTargetDict = msgMethodSigDict = nil;
    }

    return NO;
}

//
// Deallocate object: Emit destroy
//
- (void) dealloc
{
    [self emitMessage:@"destroy", self, nil]; 
    [super dealloc];
}

//
// Install new message
//
- (void) installMessage:(NSString *)message selector:(SEL)selector
{
    // is message already added?
    if(nil == [msgTargetDict objectForKey:message])
    {
        // is instance able to respond to selector?
        if([self respondsToSelector:selector])
        {
            NSMethodSignature *sig = [self methodSignatureForSelector:selector];

            const char *retType = [sig methodReturnType]; 
            
            // is prototypes return type of type BOOL?
            if(0 == strcmp(@encode(BOOL), retType))
            {
                NSUInteger  nArgs = [sig numberOfArguments];

                // is there at least one additional argument?
                if(nArgs > 2)
                {
                    const char *lastArgType = [sig getArgumentTypeAtIndex:nArgs-1]; 
                    
                    // is prototypes last argument of type id?
                    if(0 == strcmp(@encode(id), lastArgType))
                    {
                        NSMutableArray *targets = [NSMutableArray new];
                
                        // targets array
                        [msgTargetDict setObject:targets forKey:message];

                        // method signature
                        [msgMethodSigDict setObject:sig forKey:message];
                
                        NSGObjectMessageReceiver *receiver = [NSGObjectMessageReceiver new];

                        // add self as 1st receiver (can't be disconnected)
                        [receiver setTarget:self];
                        [receiver setSelector:selector];
                        
                        // set signature as 'data' parameter as a guard for 'disconnectMessage'
                        [receiver setData:sig];
                                
                        [targets addObject:receiver];
                    }else
                        NSLog(@"Trying to install message '%@', but prototypes last argument is\
                              not of type id. Aborting!\n", message);
                }else
                    NSLog(@"Trying to install message '%@', but prototype must have at least one\
                          argument of type id. Aborting!\n", message);
            }else
                NSLog(@"Trying to install message '%@', but prototypes return type is not of type BOOL.\
                      Aborting!\n", message);
        }else
            NSLog(@"Trying to install message '%@', but cannot respond to selector '%@'. Aborting!\n",
                  message, NSStringFromSelector(selector));
   }else
        NSLog(@"Trying to install existing message '%@'. Aborting!\n", message);
}

//
// Connect a message receiver before/after default class handler 
//
- (void) connectMessageReal:(NSString *)message target:(id)target
                   selector:(SEL)selector data:(id)data before:(BOOL)before
{
    NSMutableArray *targets = [msgTargetDict objectForKey:message];

    if(nil != targets) // a message is installed?
    {
        if([target respondsToSelector:selector]) // check whether target can respond to given selector?
        {
            NSMethodSignature *sig1 = [msgMethodSigDict objectForKey:message];

            // is given selector of good prototype?
            if([self compareMethodsSignature:sig1 sig2:[target methodSignatureForSelector:selector]])
            { 
                NSGObjectMessageReceiver *newReceiver = [NSGObjectMessageReceiver new];

                [newReceiver setTarget:target];
                [newReceiver setSelector:selector];
                [newReceiver setData:data];

                if(before == YES)
                {
                    //
                    // Search for default class handler and add it before
                    //
                    
                    id         receiver   = nil;
                    NSUInteger nReceivers = [targets count]; 

                    for(NSUInteger i=0; i<nReceivers; ++i) 
                    {        
                        receiver = [targets objectAtIndex:i];
               
                        if((NSMethodSignature *)[receiver data] == sig1)
                        {
                            [targets insertObject:newReceiver atIndex:i];
                            break;
                        }
                    }
                    }else
                        [targets addObject:newReceiver];
            }else
                NSLog(@"Trying to connect on message '%@' but given selector '%@' doesn't match\
                          installed one. Aborting! \n", message, NSStringFromSelector(selector));
        }else
            NSLog(@"Trying to connect on message '%@' but cannot respond to selector '%@'. Aborting!\n",
                  message, NSStringFromSelector(selector));
    }else
        NSLog(@"Trying to connect on non-existing message '%@'. Aborting!\n", message);
}

//
// Connect message receiver before default class handler 
//
- (void) connectMessage:(NSString *)message target:(id)target selector:(SEL)selector data:(id)data
{
    [self connectMessageReal:message target:target selector:selector data:data before:YES];
}

//
// Connect message receiver after default class handler 
//
- (void) connectMessageAfter:(NSString *)message target:(id)target selector:(SEL)selector data:(id)data
{
    [self connectMessageReal:message target:target selector:selector data:data before:NO];
}

//
// Disconnect target(s) from the message 
//
- (void) disconnectMessage:(NSString *)message target:(id)target
{
    NSMutableArray *targets = [msgTargetDict objectForKey:message];

    if(nil != targets)
    {
        NSEnumerator      *enumTargets    = [targets objectEnumerator]; 
        NSMutableArray    *discardedItems = [NSMutableArray new];
        NSMethodSignature *sig            = [msgMethodSigDict objectForKey:message];
        id                 receiver;
        
        while(nil != (receiver = [enumTargets nextObject]))
        {
            // do not disconnect original selector
            if(target == [receiver target] && sig != (NSMethodSignature *)[receiver data])
                [discardedItems addObject:receiver];
        }
        
        [targets removeObjectsInArray:discardedItems];
        [discardedItems release];
    }else
        NSLog(@"Trying to disconnect from non-existing message '%@'. Aborting!\n", message);
}
@end
