//
//  main.m
//  NSGObject
//
//  Created by mirza on 10/5/13.
//  Copyright (c) 2013 mirza. All rights reserved.
//

#import "NSGObject.h"
#import <stdlib.h>
#import <time.h>


/****************	Auctioneer   ****************/
@interface BidderElem : NSObject
{
    NSUInteger bid;
    id bidder;
}
@property (assign, readwrite) NSUInteger bid;
@property (assign, readwrite) id bidder;
@end

@implementation BidderElem
@synthesize bid = _bid;
@synthesize bidder = _bidder;
@end

@interface Auctioneer : NSGObject<NSCopying>
{
    @private
    NSString *item;
    NSUInteger turn;
    BidderElem *elem;
    NSMutableDictionary *bidders;
}

@property (assign, readonly) NSString *name;

// Messages
- (BOOL) bidItem:(NSString *) anItem anElem:(BidderElem *)anElem aData:(id)aData;
- (BOOL) itemSold:(NSString *) anItem anElem:(BidderElem *)anElem aData:(id)aData;
- (BOOL) announceBestBid:(NSString *) anItem anElem:(BidderElem *)anElem aTurn:(NSUInteger)aTurn aData:(id)aData;
// Methods
+ (Auctioneer *) sharedInstance;
- (void) updateItemBid:(NSString *) anItem aBid:(NSUInteger)aBid aBidder:(id)aBidder;
- (void) sellItem:(NSString *) anItem aPrice:(NSUInteger)aPrice;
@end


@implementation Auctioneer

+ (Auctioneer *) sharedInstance
{
    static Auctioneer *myInstance = nil;
        
    if (!myInstance)
    {
        myInstance = [[[self class] alloc] init];
    }
    
    return myInstance;
}

- (NSString *) name
{
    return @"Auctioneer";
}

- (id) init
{
    self = [super init];
    
    if(self != nil)
    {
        // install messages
        [self installMessage:@"bidItem" selector:@selector(bidItem:anElem:aData:)];
        [self installMessage:@"announceBestBid" selector:@selector(announceBestBid:anElem:aTurn:aData:)];
        [self installMessage:@"itemSold" selector:@selector(itemSold:anElem:aData:)];
    
        // data
        bidders = [NSMutableDictionary new];
        elem = [BidderElem new];
        turn = 0;
    }
    
    return self;
}


- (id)copyWithZone:(NSZone *)aZone
{
    return self; // do not retain here, so 'destroy' can be invoked!
}


// forward declaration
@class Buyer;

- (BOOL) bidItem:(NSString *) anItem anElem:(BidderElem *)anElem aData:(id)aData
{
    return NO;
}


- (BOOL) itemSold:(NSString *) anItem anElem:(BidderElem *)anElem aData:(id)aData
{
    NSLog(@"Item '%@' sold for %lu$ to the '%@'. \n", anItem, anElem.bid, [(Buyer *)anElem.bidder name]);
    return NO;
}


- (BOOL) announceBestBid:(NSString *) anItem anElem:(BidderElem *)anElem aTurn:(NSUInteger)aTurn aData:(id)aData
{
    NSLog(@"Announcing '%lu' time: item '%@' best bid %lu$ by '%@'. \n", aTurn,
          anItem, anElem.bid, [(Buyer *)anElem.bidder name]);
    return NO;
}


- (void) sellItem:(NSString *) anItem aPrice:(NSUInteger)aPrice
{
    NSLog(@"Selling an item '%@' at initial price of '%lu$'. \n", anItem, aPrice);

    item = anItem;

    BidderElem *firstBidder = [BidderElem new];
    firstBidder.bid = elem.bid = aPrice;
    firstBidder.bidder = elem.bidder = self;
    
    [bidders setObject:firstBidder forKey:self];
        
    while(turn < 3)
    {
        [self emitMessage:@"bidItem", anItem, elem, nil];
        [self emitMessage:@"announceBestBid", anItem, elem, ++turn, nil];
    }
    
    [self emitMessage:@"itemSold", anItem, elem, nil];
}


- (void) updateItemBid:(NSString *) anItem aBid:(NSUInteger)aBid aBidder:(id)aBidder
{
    NSLog(@"Item '%@' bidden for %lu$ by '%@'. \n", anItem, aBid, [(Buyer *)aBidder name]);

    BidderElem *_elem = [bidders objectForKey:aBidder];
 
    if(_elem == nil)
    {
        _elem = [BidderElem new];
    }
    
    _elem.bid = aBid;
    _elem.bidder = aBidder;
    
    [bidders setObject:_elem forKey:aBidder];
    
    // update best bidder
    for(id bidder in bidders)
    {
        BidderElem *e = [bidders objectForKey:bidder];
        
        if(e.bid > elem.bid)
        {
            elem.bid = e.bid;
            elem.bidder = e.bidder;
            turn = 0;
        }
    }
}


/*
 If parent messages is overriden it is mandatory
 to invoke parents implementation via super.
*/
- (BOOL) destroy:(id)object data:(id)data
{
    [bidders removeAllObjects];
    [bidders release];
    [elem release];
    return [super destroy:object data:data];
}

@end


/****************	Buyer   ****************/
@interface Buyer : NSObject<NSCopying>
{
    @private
    NSUInteger bet;
    NSUInteger totalMoney;
}

@property (assign, readwrite) NSString *name;


// Messages
- (BOOL) bidItem:(NSString *) anItem anElem:(BidderElem *)anElem aData:(id)aData;
- (BOOL) itemSold:(NSString *) anItem anElem:(BidderElem *)anElem aData:(id)aData;


// Methods
- (id) initWithData:(NSString *)aName aBet:(NSUInteger)aBet aTotalMoney:(NSUInteger)aTotalMoney;
@end


@implementation Buyer

@synthesize name = _name;

// Messages
- (BOOL) bidItem:(NSString *) anItem anElem:(BidderElem *)anElem aData:(id)aData
{    
    if(anElem.bidder != self) // won't try to compete with myself
    {
        if(anElem.bid < totalMoney && anElem.bid >= bet)
        {
            bet = anElem.bid + random() % (totalMoney - anElem.bid) + 1;
            [[Auctioneer sharedInstance] updateItemBid:anItem aBid:bet aBidder:self];
        }
    }

    return NO;
}

- (BOOL) itemSold:(NSString *) anItem anElem:(BidderElem *)anElem aData:(id)aData;
{
    if(self != anElem.bidder) // wasn't me!? Heh!
    {
        NSLog(@"%@: hey %@, you're lucky one!\n", self.name, [(Buyer *)anElem.bidder name]);
    }
        
    return NO;
}


// Methods
- (id) initWithData:(NSString *)aName aBet:(NSUInteger)aBet aTotalMoney:(NSUInteger)aTotalMoney;
{
    self = [super init];
    
    if(self != nil)
    {
        self.name = aName;
        bet = aBet;
        totalMoney = aTotalMoney;
    }
    
    return self;
}


- (id)copyWithZone:(NSZone *)aZone
{
    return [self retain];
}

@end


int main()
{
    Auctioneer *auctioneer = [Auctioneer sharedInstance];

    srandom((unsigned)time(NULL));

    NSArray *aBuyerName = [[NSArray alloc] initWithObjects:@"Steve", @"Bill", @"Larry", nil];
    Buyer *aBuyer[3];

    for(NSUInteger i=0; i<3; i++)
    {
        NSString *name = [aBuyerName objectAtIndex:i];
        aBuyer[i] = [[Buyer alloc] initWithData:name aBet:1 aTotalMoney:random() % 10 + 1000];
        [auctioneer connectMessageAfter:@"bidItem" target:aBuyer[i]
                               selector:@selector(bidItem:anElem:aData:) data:nil];
        [auctioneer connectMessageAfter:@"itemSold" target:aBuyer[i]
                               selector:@selector(itemSold:anElem:aData:) data:nil];
    }
    
    [auctioneer sellItem:@"iPhone 7" aPrice:100];
        
    // optional
    for(NSUInteger i=0; i<3; i++)
    {
        [auctioneer disconnectMessage:@"bidItem" target:aBuyer[i]];
        [auctioneer disconnectMessage:@"itemSold" target:aBuyer[i]];
    }
    
    // release everything
    [auctioneer release];
    [aBuyerName release];
    for(NSUInteger i=0; i<3; i++)
    {
        [aBuyer[i] release];
    }
        
    return 0;
}
