#import <Foundation/Foundation.h>

/*
 Ala 'GObject' with Objective-C
 
 The NSGObject is a simple dynamic message dispatch system with selectors inspired by GObject from GTK+. You start 
 by installing a message via 'installMessage' and providing a 'message' name and instance 'selector' which will 
 be used as callback prototype. Provided 'selector' is internally registered as default class receiver in a queue 
 of receivers. After installation use 'emitMessage' to emit message and invoke installed message selector/callback. 
 In order to connect more receivers use a 'connectMessage' method with witch additionally you can supply a 'data' 
 parameter. If supplied, the 'data' argument is packed last within 'emitMessage'. Beware that number of arguments 
 given to 'emitMessage' should match number of arguments minus one of installed message prototype. Every connected 
 receiver with 'connectMessage' will be called before default class handler and connected with 'connectMessageAfter' 
 will be called after default class handler. When emitting with 'emitMessage' all receivers are messaged in order and 
 their return value is processed. If receiver return NO the 'emitMessage' will continue on processing other receivers 
 until all receivers are processed, or one of receivers returns YES which will abort further processing. 
 To disconnect receiver from the 'message' queue you call 'disconnectMessage' for the given 'target'.
 Note: The BOOL is only allowed return type for message receiver prototype, but you can use an arbitrary number
 of starting arguments with enclosing of type id, for the 'data' which is a must.
*/
@interface NSGObject : NSObject
{       
    @private
    NSMutableDictionary *msgTargetDict;
    NSMutableDictionary *msgMethodSigDict;
}

/*
 installMessage:

 Install new message of 'message' name and bind given instance 'selector' to it. The 'selector' is 
 tested within instance and should be responsive. Only selectors which return BOOL are allowed,
 where at least one and last argument must be of type id. The given selector is stored as method 
 signature where any connected receiver must match installed prototype.
 The given selector is installed as default class receiver which cannot be disconnected.
 To invoke receiver use 'emitMessage'.
*/
- (void) installMessage:(NSString *)message selector:(SEL)selector;     

/*
 connectMessage:

 Connect receiver to installed 'message' by providing 'target' and 'selector' to call on with
 'data' as optional parameter. The given 'selector' must match installed one for the 'message'.
 The receiver will be called before default class handler. The receivers are called in order
 how they are connected.
*/
- (void) connectMessage:(NSString *)message target:(id)target selector:(SEL)selector data:(id)data;

/*
 connectMessageAfter:

 Connect receiver to installed 'message' by providing 'target' and 'selector' to call on with
 'data' as optional parameter. The given 'selector' must match installed one for the 'message'.
 The receiver will be called after default class handler. The receivers are called in order
 how they are connected.
*/
- (void) connectMessageAfter:(NSString *)message target:(id)target selector:(SEL)selector data:(id)data;

/*
 disconnectMessage:

 Disconnect receiver 'target' from the 'message'. If there is more than one receiver for the target 
 registered for the given 'message' all of them will be disconnected.
 Note that default class handler will not be disconnected.
*/
- (void) disconnectMessage:(NSString *)message target:(id)target;

/*
 emitMessage:

 Emit 'message' with arbitrary number of arguments. Note that argument number must match installed
 minus one, which is reserved to 'data' and is provided with 'connectMessage' and 'connectMessageAfter'.
 The receivers in queue are messaged one by one until all are processed or one of callee returns
 YES which aborts further processing. 
*/
- (void) emitMessage:(NSString *)message, ...; 

/*
 Messages section:

 Note:  The particular message is intended to invoke manually, as normal method, only if you're
        overriding a parent one. In such case you must invoke parent selector to properly chain
        selectors and no message is emitted.
        You must use 'emitMessage' in order to invoke message handler(s), but never from message
        handler itself because you will end up with infinite recursion (stack overflow).

 Proposition: Let the message name be derived from a method name. This is not strictly but it will
              introduce simplicity if it is followed. Therefore, a message name for 'destroy:data:' 
              is 'destroy'. Please follow this intention.

 destroy:

 This message is sent from NSGObject's 'dealloc' method. In 'destroy', both member dict's are released.
 If you override this method in sub-class you must invoke parents 'destroy' with super. Or if you queue
 before super class (with 'connectMessage') it is advisable to return NO in order to pass processing to 
 the super class. Otherwise, you will end up with leak as dict's are not subscribed to NSAutoreleasePool. 
*/
- (BOOL) destroy:(id)object data:(id)data;

@end
